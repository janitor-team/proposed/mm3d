mm3d (1.3.13-1) unstable; urgency=medium

  * New upstream version.
  * Bump debhelper version to 13, drop d/compat.
  * Bump standards version to 4.6.0.
  * Update maintainer address.
  * d/clean: updated.
  * d/control: added Rules-Requires-Root.
  * d/copyright: modernised and updated years.
  * d/patches: not applying anymore dbgsym and fix-spelling.

 -- Gürkan Myczko <tar@debian.org>  Wed, 22 Dec 2021 05:30:37 +0100

mm3d (1.3.12-2) unstable; urgency=medium

  * Bump standards version to 4.4.1.
  * Add Vcs fields.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 31 Jan 2020 16:06:18 +0100

mm3d (1.3.12-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 09 Oct 2019 15:49:51 +0200

mm3d (1.3.12~rc1-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.4.0.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 01 Oct 2019 13:53:06 +0200

mm3d (1.3.11-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 03 Jan 2019 06:46:44 +0100

mm3d (1.3.10-3) unstable; urgency=medium

  * Maybe fix GNU/Hurd building.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 12 Dec 2018 09:06:25 +0100

mm3d (1.3.10-2) unstable; urgency=medium

  * Fix GNU/Hurd building.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 11 Dec 2018 11:17:07 +0100

mm3d (1.3.10-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.2.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 08 Sep 2018 13:20:41 +0200

mm3d (1.3.9+git20180220-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 20 Feb 2018 16:44:47 +0100

mm3d (1.3.9-2) unstable; urgency=medium

  * debian/copyright: improved.
  * debian/rules: profiling turned off. (Closes: #517564, #515140)
  * debian/control: update build-deps. (Closes: #889005)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 05 Feb 2018 14:25:31 +0100

mm3d (1.3.9-1) unstable; urgency=medium

  * New upstream version. (Closes: #875048, #494967)
  * debian/mm3d.1: dropped.
  * debian/mm3d.desktop: dropped.
  * debian/patches: dropped.
  * Homepages updated.
  * debian/watch: updated.
  * Bump standards version to 11.
  * Bump debhelper version to 4.1.3.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 26 Jan 2018 20:35:15 +0100

mm3d (1.3.7-3) unstable; urgency=medium

  * QA upload.
  * Reorder command line arguments to fix FTBFS with --as-needed linker option.
  * Bump standards version.
  * Don't ship mimelnk file anymore. Closes: #875518.

 -- Matthias Klose <doko@debian.org>  Mon, 06 Nov 2017 16:23:43 +0100

mm3d (1.3.7-2) unstable; urgency=medium

  * Orphan the package.
  * Fix an underquoted autoconf macro.
  * Convert to dh (9).
    + dh_desktop is no more (Closes: #817301).
  * Rebuild autoconfage at build time.
  * Remove debian/menu.

 -- Adam Borowski <kilobyte@angband.pl>  Mon, 04 Apr 2016 04:17:05 +0200

mm3d (1.3.7-1.4) unstable; urgency=low

  * Non-maintainer upload.
  * Fix unreported FTBFS due to multi-arched Qt:
    - build depend on pkg-config
    - use it to set Qt's libdir in debian/rules
  * Fix "ftbfs with GCC-4.7":
    - src/libmm3d/misc.cc: add missing include
    - src/libmm3d/sorted_list.h: add this-> qualifiers
    (Closes: #667284)

 -- gregor herrmann <gregoa@debian.org>  Wed, 09 May 2012 17:28:22 +0200

mm3d (1.3.7-1.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: cmdmgr.cc:27:47: error: 'NULL' was not declared in this
    scope": add patch from peter green (include missing header).
    Closes: #625117

 -- gregor herrmann <gregoa@debian.org>  Wed, 21 Dec 2011 14:06:49 +0100

mm3d (1.3.7-1.2) unstable; urgency=low

  * Non-maintainer upload
  * Fix FTBFS due to newer GCC compiler (Closes: #505626)
    Thanks to Martin Michlmayr

 -- Steffen Joeris <white@debian.org>  Fri, 22 Jan 2010 23:08:35 +0100

mm3d (1.3.7-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS bug due to wrong Build-Depends -- still QT3 but new upstream
    moved to QT4. Removed useless dependencies on various X libraries.
    (Closes: #490331, #489838)

 -- Ludovico Gardenghi <garden@acheronte.it>  Tue, 21 Oct 2008 01:00:14 +0200

mm3d (1.3.7-1) unstable; urgency=low

  * New upstream version.
  * Updated my email address.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 02 Jul 2008 08:28:38 +0200

mm3d (1.3.6-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix g++-4.3 FTBFS thanks to Cyril Brulebois (Closes: 454828).

 -- Pierre Habouzit <madcoder@debian.org>  Sun, 16 Mar 2008 22:40:22 +0000

mm3d (1.3.6-1) unstable; urgency=low

  * New upstream version.
  * debian/watch: Added.
  * debian/control:
    + Updated standards version.
    + Moved homepage field.
  * debian/menu: Updated.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 14 Jan 2008 06:22:24 +0100

mm3d (1.3.5-1) unstable; urgency=low

  * New upstream version.
  * Added desktop file.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 17 Jul 2007 01:52:21 +0200

mm3d (1.3.4-1) unstable; urgency=low

  * Initial release. (Closes: #426994)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 31 May 2007 21:28:35 +0200
